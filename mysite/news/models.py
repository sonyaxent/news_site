from django.db import models
from django.urls import reverse


class News(models.Model):
    title = models.CharField(max_length=150, verbose_name='News Header')
    content = models.TextField(blank=True, verbose_name='Content of our news')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Was written on')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Was updated at')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d', verbose_name='The photo', blank=True)
    is_published = models.BooleanField(default=True, verbose_name='Was published')
    category = models.ForeignKey('Category', on_delete=models.PROTECT, verbose_name="Category")
    views = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('view_news', kwargs={'pk': self.pk})


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'All news'
        ordering = ['created_at']


class Category(models.Model):
    title = models.CharField(max_length=150, db_index=True, verbose_name='News Category')

    def get_absolute_url(self):
        return reverse('category', kwargs={'category_id': self.pk})


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['title']
